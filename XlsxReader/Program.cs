﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.XPath;
using Excel = Microsoft.Office.Interop.Excel;

using Ini;
using NDesk.Options;
using AE.Net.Mail;
using AE.Net.Mail.Imap;

namespace XlsxReader
{
    class Program
    {
        static string defaultConfig = "CurrencyExposureReport.ini";
        static string config = null;

        static void LoadLatestFile(string ip, string username, string password, string subjectPattern, string outputDirectory)
        {
            try
            {
                using (ImapClient client = new ImapClient(ip, username, password, AuthMethods.Login, 993, true))
                {
                    Console.WriteLine("Logged in: " + username);
                    client.SelectMailbox("INBOX");
                    DateTime currentDate = DateTime.Now;
                    int currentMessageNo = client.GetMessageCount();
                    Regex subjectRegex = new Regex(subjectPattern);
                    MailMessage message = null;
                    while (currentDate >= DateTime.Now.AddDays(-7))
                    {
                        --currentMessageNo;
                        message = client.GetMessage(currentMessageNo, true, false);  // Get Message Headers
                        currentDate = message.Date;
                        if (subjectRegex.IsMatch(message.Subject))
                        {
                            Console.WriteLine("Found Message: " + message.Subject + " | " + message.From.Address + " | " + message.Date);
                            break;
                        }
                    }
                    if (!subjectRegex.IsMatch(message.Subject))
                    {
                        Console.WriteLine("No matching messages found.");
                    }
                    message = client.GetMessage(currentMessageNo, false, false);  // Get Mesasge Contents
                    foreach (Attachment attachment in message.Attachments)
                    {
                        Console.WriteLine("Saving Attachment: " + attachment.Filename);
                        attachment.Save(Path.Combine(outputDirectory, attachment.Filename));
                    }
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }
        }

        static string GetLatestFile(string filePattern)
        {
            string directory = Path.GetDirectoryName(filePattern);
            string fileSearchPattern = Path.GetFileName(filePattern);
            try
            {
                return new DirectoryInfo(directory).GetFiles(fileSearchPattern).OrderByDescending(f => f.CreationTime).ToList()[0].FullName;
            }
            catch (ArgumentOutOfRangeException e)
            {
                return null;
            }
        }

        static Dictionary<string, double> ReadFromXlsx(string filename, string sheetname, string tableStartRange, string rowKey, string[] columnKeys, out Dictionary<string, double> rates)
        {
            // Init
            Excel.Application app = new Excel.Application();
            app.EnableEvents = true;
            app.Visible = false;
            app.Interactive = false;
            app.ScreenUpdating = false;

            Excel.Workbook wb = app.Workbooks.Add(filename);  // .Open(filename, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            app.Visible = false;  // 
            Excel.Worksheet ws = (Excel.Worksheet)wb.Sheets[sheetname];
            Excel.Range tableStart = ws.get_Range(tableStartRange, Type.Missing);
            int keyRow = tableStart.Row;
            while (keyRow <= ws.UsedRange.Rows.Count)
            {
                Excel.Range cell = (Excel.Range)ws.Cells[keyRow, tableStart.Column];
                string cellValue = cell.Value2 == null ? "" : cell.Value2.ToString();
                if (cellValue == rowKey)
                {
                    break;
                }
                ++keyRow;
            }
            Dictionary<string, int> keyColumns = new Dictionary<string, int>();
            int tmpColumn = tableStart.Column;
            while (tmpColumn <= ws.UsedRange.Columns.Count)
            {
                Excel.Range cell = (Excel.Range)ws.Cells[tableStart.Row, tmpColumn];
                string cellValue = cell.Value2 == null ? "" : cell.Value2.ToString();
                if (columnKeys.Contains(cellValue))
                {
                    if (!keyColumns.ContainsKey(cellValue))
                    {
                        keyColumns[cellValue] = tmpColumn;
                    }
                }
                ++tmpColumn;
            }

            // Lookup Values
            Dictionary<string, double> results = new Dictionary<string, double>();
            if (keyRow <= ws.UsedRange.Rows.Count)
            {
                foreach (string key in keyColumns.Keys)
                {
                    Excel.Range cell = (Excel.Range)ws.Cells[keyRow, keyColumns[key]];
                    try
                    {
                        results[key] = (double)cell.Value2;
                    }
                    catch (InvalidCastException e) { }
                }
            }

            // Lookup Exchange Rates
            Dictionary<string, double> exchangeRates = new Dictionary<string, double>();
            if (keyRow <= ws.UsedRange.Rows.Count)
            {
                foreach (string key in keyColumns.Keys)
                {
                    Excel.Range cell = (Excel.Range)ws.Cells[keyRow, keyColumns[key] + 1];
                    try
                    {
                        exchangeRates[key] = (double)cell.Value2 / results[key];
                    }
                    catch (IndexOutOfRangeException e) { }
                    catch (InvalidCastException e) { }
                }
            }
            rates = exchangeRates;

            // Cleaning Up
            app.Interactive = true;
            app.ScreenUpdating = true;
            wb.Close(false, Type.Missing, Type.Missing);
            app.Quit();
            return results.Count > 0 ? results : null;
        }

        static Dictionary<string, Dictionary<DateTime, double>> GetExchangeRatesFromDB(string ip, string db, string username, string password, string[] currencies)
        {
            string connectionString = "Data Source=" + ip + ";Initial Catalog=" + db + ";User ID=" + username + ";Password=" + password + ";Connection Timeout=60";  // Provider=SQLOLEDB;
            string cmd = "SELECT TOP " + 30 * currencies.Length + " b.[crcyName] AS currency, a.[d] AS value_date, exp(log(a.[usdRate]) * b.[mul]) AS rate FROM [dbo].[crcyD] a INNER JOIN [dbo].[Crcy] b ON a.[crcyId] = b.[crcyId] WHERE b.[crcyName] IN (" + String.Join(", ", currencies.Select(item => "'" + item + "'").ToArray()) + ") ORDER BY a.[d] DESC, b.[crcyName] FOR XML RAW;";
            SqlConnection sqlCon = new SqlConnection(connectionString);
            try
            {
                sqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand(cmd, sqlCon);
                sqlCmd.CommandTimeout = 60;
                XmlReader xmlReader = sqlCmd.ExecuteXmlReader();
                XPathDocument xp = new XPathDocument(xmlReader);
                XPathNavigator xn = xp.CreateNavigator();
                XmlDocument xd = new XmlDocument();
                XmlNode root = xd.CreateElement("root");
                root.InnerXml = xn.OuterXml;
                Dictionary<string, Dictionary<DateTime, double>> results = new Dictionary<string, Dictionary<DateTime, double>>();
                foreach (XmlNode node in root.ChildNodes)
                {
                    try
                    {
                        string currency = node.Attributes.GetNamedItem("currency").Value.Trim();
                        if (!results.ContainsKey(currency))
                        {
                            results[currency] = new Dictionary<DateTime,double>();
                        }
                        DateTime valueDate = DateTime.Parse(node.Attributes.GetNamedItem("value_date").Value);
                        double rate = double.Parse(node.Attributes.GetNamedItem("rate").Value);
                        if (!results[currency].ContainsKey(valueDate))
                        {
                            results[currency][valueDate] = rate;
                        }
                    }
                    catch (NullReferenceException ex) { }
                    catch (FormatException ex) { }
                }
                return results;
            }
            catch (SqlException e)
            {
                Console.Error.WriteLine("SQL Connection Error");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return null;
            }
        }

        static Dictionary<string, List<double>> GetExchangeRateCalculations(Dictionary<string, Dictionary<DateTime, double>> exchangeRateSeries)
        {
            Dictionary<string, List<double>> results = new Dictionary<string, List<double>>();
            foreach (string currency in exchangeRateSeries.Keys)
            {
                if (exchangeRateSeries[currency].Count == 0)
                {
                    continue;
                }
                List<DateTime> valueDates = exchangeRateSeries[currency].Keys.OrderByDescending(d => d).ToList();
                DateTime lastValueDate = valueDates[0];
                DateTime oneDayAgo = valueDates[1];
                DateTime oneWeekAgo = valueDates.Where(d => d <= lastValueDate.AddDays(-7)).Max();
                DateTime oneMonthAgo = valueDates.Where(d => d <= lastValueDate.AddMonths(-1)).Max();
                List<double> calcs = new List<double>();
                calcs.Add(exchangeRateSeries[currency][lastValueDate]);
                calcs.Add(exchangeRateSeries[currency][oneDayAgo]);
                calcs.Add(exchangeRateSeries[currency][oneWeekAgo]);
                calcs.Add(exchangeRateSeries[currency][oneMonthAgo]);
                results[currency] = calcs;
            }
            return results;
        }

        static string FormatData(Dictionary<string, double> exposures, Dictionary<string, List<double>> exchangeRateCalculations, Dictionary<string, string> mappings, string referenceFile)
        {
            string html = "<p><h2>Date: " + DateTime.Today.ToString("yyyy-MM-dd") + "</h2></p>";
            html += "<p><h3>Currency Exposure Report: " + referenceFile + "</h3></p>";
            html += "<p><table border=1 cellpadding=1><tr><b><td>Currency</td><td>Net Exposure (Local)</td><td>Net Exposure (USD)</td><td>Latest Exchange Rate</td><td>Exchange Rate 1 Day Ago</td><td>Exchange Rate 1 Week Ago</td><td>Exchange Rate 1 Month Ago</td><b></tr>";
            foreach (string currency in exposures.Keys)
            {
                List<double> rates = null;
                if (exchangeRateCalculations.ContainsKey(currency))
                {
                    rates = exchangeRateCalculations[currency];
                }
                else if (mappings.ContainsKey(currency) && exchangeRateCalculations.ContainsKey(mappings[currency]))
                {
                    rates = exchangeRateCalculations[mappings[currency]];
                }
                if (rates == null)
                {
                    rates = new List<double>();
                    rates.AddRange(Enumerable.Repeat(double.NaN, 4));
                }
                html += "<tr><td>" + currency + "</td><td>" + exposures[currency].ToString("#,#.00") + "</td><td>" + (exposures[currency] / rates[0]).ToString("#,#.00") + "</td>";
                foreach (double rate in rates)
                {
                    html += "<td>" + rate.ToString("0.00000") + "</td>";
                }
                html += "</tr>";
            }
            html += "</table></p>";
            return html;
        }

        static void SendEmail(string ip, string username, string password, string mailingList, string body)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.IsBodyHtml = true;
            message.To.Add(mailingList);
            message.Subject = "Currency Exposure Report " + DateTime.Today.ToString("yyyy-MM-dd");
            message.From = new System.Net.Mail.MailAddress(username);
            message.Body = body;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(ip, 587);
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential(username, password);
            int retryCount = 10;
            while (retryCount > 0)
            {
                try
                {
                    smtp.Send(message);
                    break;
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e.Message);
                    Console.Error.WriteLine(e.StackTrace);
                    --retryCount;
                    if (retryCount > 0)
                    {
                        Console.Error.WriteLine("Mail not sent. Retrying ...");
                    }
                }
            }
            if (retryCount > 0)
            {
                Console.WriteLine("Mail sent.");
            }
            else
            {
                Console.Error.WriteLine("mail not sent. Please check manually.");
            }
        }

        static void Main(string[] args)
        {
            // Command-line Options (Deprecated)
            OptionSet options = new OptionSet().Add("c=|config=", c => config = c);
            options.Parse(args);
            if (config == null)
            {
                config = Path.Combine(Directory.GetCurrentDirectory(), defaultConfig);
            }

            // Read from ini File & Check
            IniFile ini = new IniFile(config);
            string filePattern = ini.IniReadValue("main", "file");
            string sheetName = ini.IniReadValue("main", "sheet");
            string tableStartRange = ini.IniReadValue("main", "tablestartrange");
            string rowKey = ini.IniReadValue("main", "rowkey");
            string[] columnKeys = ini.IniReadValue("main", "columnkeys").Split(',');
            string[] mappings = ini.IniReadValue("main", "mappings").Split(',');
            string ip = ini.IniReadValue("db", "ip");
            string db = ini.IniReadValue("db", "database");
            string username = ini.IniReadValue("db", "username");
            string password = ini.IniReadValue("db", "password");
            string imapIp = ini.IniReadValue("imap", "ip");
            string imapUsername = ini.IniReadValue("imap", "username");
            string imapPassword = ini.IniReadValue("imap", "password");
            string imapSubjectPattern = ini.IniReadValue("imap", "subject");
            string mailIp = ini.IniReadValue("smtp", "ip");
            string mailUsername = ini.IniReadValue("smtp", "username");
            string mailPassword = ini.IniReadValue("smtp", "password");
            string mailRecipients = ini.IniReadValue("smtp", "recipients");
            if (filePattern == "" || sheetName == "" || tableStartRange == "" || rowKey == "" || columnKeys[0] == "" || ip == "" || db == "" || username == "" || password == "" || imapIp == "" || imapUsername == "" || imapPassword == "" || imapSubjectPattern == "" || mailIp == "" || mailUsername == "" || mailPassword == "" || mailRecipients == "")
            {
                if (filePattern == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [main] <file>");
                }
                if (sheetName == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [main] <sheet>");
                }
                if (tableStartRange == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [main] <tablestartrange>");
                }
                if (rowKey == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [main] <rowkey>");
                }
                if (columnKeys[0] == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [main] <columnkeys>");
                }
                if (ip == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [db] <ip>");
                }
                if (db == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [db] <database>");
                }
                if (username == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [db] <username>");
                }
                if (password == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [db] <password>");
                }
                if (imapIp == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [imap] <ip>");
                }
                if (imapUsername == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [imap] <username>");
                }
                if (imapPassword == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [imap] <password>");
                }
                if (imapSubjectPattern == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [imap] <subject>");
                }
                if (mailIp == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [smtp] <ip>");
                }
                if (mailUsername == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [smtp] <username>");
                }
                if (mailPassword == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [smtp] <password>");
                }
                if (mailRecipients == "")
                {
                    Console.Error.WriteLine("Error in Configuration File: Missing [smtp] <recipients>");
                }
                Environment.Exit(0);
            }

            // Attempts to Load Latest Excel File from Email
            // Can be skipped if IMAP port 993 is not open
            LoadLatestFile(imapIp, imapUsername, imapPassword, imapSubjectPattern, Path.GetDirectoryName(filePattern));

            // Read Latest Excel File
            string filename = GetLatestFile(filePattern);
            if (filename == null)
            {
                Console.Error.WriteLine("No files matching {0}.", filePattern);
                Environment.Exit(0);
            }
            Console.WriteLine(filename);
            Dictionary<string, double> exchangeRates;
            Dictionary<string, double> results = ReadFromXlsx(filename, sheetName, tableStartRange, rowKey, columnKeys, out exchangeRates);
            if (results == null)
            {
                Console.Error.WriteLine("Error in Excel file contents");
                Environment.Exit(0);
            }

            // Get Exchange Rates from DB
            Console.WriteLine("Getting Exchange Rates from DB ...");
            Dictionary <string, string> currencyMappings = new Dictionary<string,string>();
            foreach (string mapping in mappings)
            {
                string[] tokens = mapping.Split('|');
                if (tokens.Length != 2)
                {
                    continue;
                }
                if (!currencyMappings.ContainsKey(tokens[0]))
                {
                    currencyMappings[tokens[0]] = tokens[1];
                }
            }
            HashSet<string> currencies = new HashSet<string>();
            foreach (string currency in columnKeys)
            {
                currencies.Add(currencyMappings.ContainsKey(currency) ? currencyMappings[currency] : currency);
            }
            Dictionary<string, Dictionary<DateTime, double>> exchangeRateSeries = GetExchangeRatesFromDB(ip, db, username, password, currencies.ToArray());

            // Produce Exchange Rate Summary
            Dictionary<string, List<double>> exchangeRateCalculations = GetExchangeRateCalculations(exchangeRateSeries);

            // Format and Send Email
            string html = FormatData(results, exchangeRateCalculations, currencyMappings, Path.GetFileName(filename));
            SendEmail(mailIp, mailUsername, mailPassword, mailRecipients, html);
        }
    }
}
